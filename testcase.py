from selenium import webdriver


class TestCase:
    """Template for testcases"""

    def __init__(self):
        self.URL = "https://www.overleaf.com/"
        self.USER = {"email": "doublevkaytester1@gmail.com", "password": "daylamatkhaumanh@123", "session": {'name': 'overleaf_session2', 'value': 's%3AzITVWbQzqeQHF4dCAoBks1xCYheblozk.fHQGwJrfVXuDxhfHnvnskM7ihEnWjGxxUBq%2BONgK69g', 'path': '/', 'domain': '.overleaf.com', 'secure': True, 'httpOnly': True, 'expiry': 1636372818, 'sameSite': 'Lax'}}
        self.PLAYGROUND = "https://www.overleaf.com/project/6183f7434634c8fdd3596db0"
        self.driver = webdriver.Firefox()

    def login(self, newSession=False):
        self.driver.get(self.URL)
        self.driver.delete_all_cookies()

        if newSession:
            # Enter login credentials
            self.driver.get(self.URL + "login")
            email = self.driver.find_element_by_id("email")
            email.clear()
            email.send_keys(self.USER["email"])
            password = self.driver.find_element_by_id("password")
            password.clear()
            password.send_keys(self.USER["password"])
        else:
            # Reuse the sessions
            self.driver.add_cookie(self.USER["session"])
            self.driver.get(self.URL + "project")
