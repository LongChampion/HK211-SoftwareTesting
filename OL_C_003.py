from testcase import TestCase
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from time import sleep


class OL_C_003(TestCase):
    def __init__(self):
        TestCase.__init__(self)
        print("[*] Testcase created!")

    def run(self):
        self.login()
        self.driver.get(self.PLAYGROUND)

        # Click review button
        sleep(5)
        reviewBtn = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[3]/a[1]")
        reviewBtn.click()

        # Click resolve icon
        sleep(1)
        icon = self.driver.find_element(By.XPATH, '//*[@id="review-panel"]/div[2]/resolved-comments-dropdown/div/a')
        icon.click()

        try:
            # Delete the first resolved comment
            sleep(1)
            deleteBtn = self.driver.find_element(By.XPATH, '//*[@id="review-panel"]/div[2]/resolved-comments-dropdown/div/div[2]/div/resolved-comment-entry[1]/div/div[2]/a[2]')
            deleteBtn.click()

        except:
            raise Exception("[-] No resolved comment to delete!")

        print("[+] Testcase completed!")
