from OL_C_001 import OL_C_001
from OL_C_002 import OL_C_002
from OL_C_003 import OL_C_003
from OL_C_004 import OL_C_004
from OL_C_005 import OL_C_005
from OL_C_006 import OL_C_006

if __name__ == "__main__":
    testcase = OL_C_006()
    testcase.run()
