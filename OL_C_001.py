from testcase import TestCase
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from time import sleep


class OL_C_001(TestCase):
    def __init__(self):
        TestCase.__init__(self)
        print("[*] Testcase created!")

    def run(self):
        self.login()
        self.driver.get(self.PLAYGROUND)

        # Click review button
        sleep(5)
        reviewBtn = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[3]/a[1]")
        reviewBtn.click()

        # Select all document
        sleep(1)
        content = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/main/div[2]/div[1]/div[1]/div[4]/div/div[2]/textarea")
        content.send_keys(Keys.CONTROL, 'a')

        for idx in range(1, 10):
            try:
                # Set parent XPATH
                XPATH = "/html/body/div[2]/div[3]/div[1]/main/div[2]/div[1]/div[1]/div[5]/div[3]/div/div[{}]/div/add-comment-entry/div/div[2]".format(idx)

                # Click new comment button
                sleep(1)
                newCommentBtn = self.driver.find_element(By.XPATH, XPATH + "/a")
                newCommentBtn.click()

                # Enter comment
                sleep(1)
                commentArea = self.driver.find_element(By.XPATH, XPATH + "/div/div[1]/textarea")
                commentArea.click()
                commentArea.send_keys("This is my comment!")

                # Click comment button (to commit)
                sleep(1)
                commentBtn = self.driver.find_element(By.XPATH, XPATH + "/div/div[2]/button[2]")
                commentBtn.click()

                break
            except:
                pass
        else:
            raise Exception("[-] Couldn't find New Comment button!")

        print("[+] Testcase completed!")
