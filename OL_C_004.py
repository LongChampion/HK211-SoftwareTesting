from testcase import TestCase
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from time import sleep


class OL_C_004(TestCase):
    def __init__(self):
        TestCase.__init__(self)
        print("[*] Testcase created!")

    def run(self):
        self.login()
        self.driver.get(self.PLAYGROUND)

        # Click review button
        sleep(5)
        reviewBtn = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[3]/a[1]")
        reviewBtn.click()

        try:
            # Delete the first comment in first thread
            sleep(1)
            deleteBtn = self.driver.find_element(By.XPATH, '//*[@id="review-panel"]/div[3]/div/div/div/comment-entry/div/div[3]/div[1]/div[1]/div/span[2]/span/a')
            deleteBtn.click()

            # Comfirm
            comfirmBtn = self.driver.find_element(By.XPATH, '//*[@id="review-panel"]/div[3]/div/div/div/comment-entry/div/div[3]/div[1]/div[1]/div/span/a[1]')
            comfirmBtn.click()
        except:
            raise Exception("[-] No comment to delete!")

        print("[+] Testcase completed!")
