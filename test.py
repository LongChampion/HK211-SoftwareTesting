import unittest, time
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from random import randrange
from fake_useragent import UserAgent
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import random
import string
import re


def random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str


# Config
URL = "https://www.overleaf.com/"
LOGIN_METHOD = "SESSION"  # SESSION or NORMAL
ENVIROMENT_TEST = "CHROME"
WEB_DRIVER_PATH_CHROME = "./web_driver/chromedriver.exe"
WEB_DRIVER_PATH_FIREFOX = "./web_driver/geckodriver.exe"

USER1 = {"username": "doublevkaytester1@gmail.com", "password": "daylamatkhaumanh@123", "session": {'name': 'overleaf_session2', 'value': 's%3Ap2ZqPYYwMPuK8YY2Yo1lXzriB1k9PwQu.wDzV5k%2B1HYSudJUhTrmBdJ4Jak8GwJvWpK8AqMIWdC0', 'path': '/', 'domain': '.overleaf.com', 'secure': True, 'httpOnly': True, 'expiry': 1936372818, 'sameSite': 'Lax'}}

USER2 = {"username": "doublevkaytester2@gmail.com", "password": "daylamatkhaumanh@123", "session": {'name': 'overleaf_session2', 'value': 's%3AYW5XSCZL4s_OFbon4uNiLO-Tc4ZYRt79.UJmw%2B8Q0yV6Fr8b44U42ZRfDwpCCirZ3zcTpJdd8VDI', 'path': '/', 'domain': '.overleaf.com', 'secure': True, 'httpOnly': True, 'expiry': 1936372818, 'sameSite': 'Lax'}}


USER3 = {"username": "doublevkaytester3@gmail.com", "password": "daylamatkhaumanh@123", "session": {'name': 'overleaf_session2', 'value': 's%3Ab8SYgRtSvn59jL78hMJV6I6b-dEdfj2_.nqFbCkF%2FPuj1uUhmpShYDEWOpokTheb59DEZrR2mPIM', 'path': '/', 'domain': '.overleaf.com', 'secure': True, 'httpOnly': True, 'expiry': 1936372818, 'sameSite': 'Lax'}}


class PythonOrgSearch(unittest.TestCase):
    def set_viewport_size(self, width, height):
        window_size = self.driver.execute_script(
            """
        return [window.outerWidth - window.innerWidth + arguments[0],
          window.outerHeight - window.innerHeight + arguments[1]];
        """,
            width,
            height,
        )
        self.driver.set_window_size(*window_size)

    def setUp(self):
        if ENVIROMENT_TEST == "FIREFOX":
            self.driver = webdriver.Firefox(executable_path=WEB_DRIVER_PATH_FIREFOX)
        else:
            self.driver = webdriver.Chrome(executable_path=WEB_DRIVER_PATH_CHROME)
        self.set_viewport_size(randrange(1000, 1200), randrange(500, 1200))
        self.PLAYGROUND = "https://www.overleaf.com/project/6183f7434634c8fdd3596db0"
        self.driver.implicitly_wait(10)
        useragent = UserAgent()
        profile = webdriver.FirefoxProfile()
        profile.set_preference("general.useragent.override", useragent.random)

    def interact(self):
        while True:
            pass

    def login(self, user):
        if LOGIN_METHOD == "SESSION":
            self.driver.get(URL)
            self.driver.delete_all_cookies()
            self.driver.add_cookie(user["session"])
            self.driver.get(URL)
        else:
            driver = self.driver
            driver.get(URL + 'login')
            sleep(randrange(2, 5))
            ele_username = driver.find_element(By.XPATH, '//*[@id="email"]')
            ele_username.send_keys(user["username"])
            sleep(randrange(1, 3))
            ele_password = driver.find_element(By.XPATH, '//*[@id="password"]')
            ele_password.send_keys(user["password"])
            ele_submit = driver.find_element(By.XPATH, '//*[@id="main-content"]/div[1]/form/div[5]/button')
            sleep(randrange(3, 10))
            ele_submit.click()
            sleep(5)
        # self.interact()

    def logout(self):
        self.driver.delete_all_cookies()

    def tearDown(self):
        self.driver.quit()

    def create_project(self, project_name=""):
        driver = self.driver
        if project_name == "":
            project_name = random_string(10)
        ele_new_project = driver.find_element(By.XPATH, '/html/body/main/div[2]/div/div/div[1]/aside/div[1]/a')
        ele_new_project.click()
        ele_blank_project = driver.find_element(By.XPATH, '/html/body/ul/li[1]/a')
        ele_blank_project.click()
        ele_project_name = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/form/input')
        # print(project_name)
        ele_project_name.send_keys(project_name)
        ele_create = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[3]/button[2]/span[1]')
        ele_create.click()
        sleep(3)
        return driver.current_url

    def invite_to_project(self, project_url, collaborator, pri="VIEW"):
        driver = self.driver
        driver.get(project_url)
        ele_share = driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[3]/a[2]/p')
        ele_share.click()
        ele_collaborator = driver.find_element(By.XPATH, '//*[@id="downshift-0-input"]')
        ele_collaborator.send_keys(collaborator)
        ele_pri_selector = Select(driver.find_element(By.CLASS_NAME, 'privileges'))
        if pri == "VIEW":
            ele_pri_selector.select_by_value('readOnly')
        else:
            ele_pri_selector.select_by_value('readAndWrite')
        sleep(1)
        ele_share_submit = driver.find_element(By.XPATH, "/html/body/div[3]/div[2]/div/div/div[2]/div/div[1]/form/div[2]/div/button")
        ele_share_submit.click()
        ele_share_submit.click()

    def turn_on_link_sharing(self, project_url):
        driver = self.driver
        driver.get(project_url)
        ele_share = driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[3]/a[2]/p')
        ele_share.click()
        ele_turn_on_link_sharing = driver.find_element(By.XPATH, "//*[contains(text(), 'Turn on link sharing')]")
        ele_turn_on_link_sharing.click()
        sleep(1)
        link_view = driver.find_elements(By.CSS_SELECTOR, '.access-token')[0].text
        link_edit = driver.find_elements(By.CSS_SELECTOR, '.access-token')[1].text
        return (link_view, link_edit)

    def join_project(self):
        driver = self.driver
        driver.get(URL + 'project')
        lst_ele_join_project = driver.find_elements(By.XPATH, "//*[contains(text(), 'Join Project')]")
        for ele in lst_ele_join_project:
            ele.click()
        sleep(1)

    def create_project_same_name(self):
        driver = self.driver
        ele_new_project = driver.find_element(By.XPATH, '/html/body/main/div[2]/div/div/div[1]/aside/div[1]/a')
        ele_new_project.click()
        ele_blank_project = driver.find_element(By.XPATH, '/html/body/ul/li[1]/a')
        ele_blank_project.click()
        ele_project_name = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/form/input')
        project_name = driver.find_element(By.XPATH, '/html/body/main/div[2]/div/div/div[2]/div[3]/div/div/ul/table/tbody/tr[2]/td[1]/div/span/a').get_attribute('innerHTML')
        ele_project_name.send_keys(project_name)
        sleep(3)
        return driver.current_url

    def download_project(self):
        driver = self.driver
        ele_download = driver.find_element(By.XPATH, '/html/body/main/div[2]/div/div/div[2]/div[3]/div/div/ul/table/tbody/tr[2]/td[4]/div/button[2]/i')
        ele_download.click()
        sleep(3)

    def upload_project(self):
        driver = self.driver
        ele_new_project = driver.find_element_by_xpath('/html/body/main/div[2]/div/div/div[1]/aside/div[1]/a')
        ele_new_project.click()
        ele_upload_project = driver.find_element_by_xpath('/html/body/ul/li[3]/a')
        ele_upload_project.click()
        ele_upload_file = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/div/div/div[2]/input')
        ele_upload_file.send_keys(os.getcwd() + '\\files\\Sample_Project.zip')

    def delete_project(self):
        driver = self.driver
        ele_delete = driver.find_element(By.XPATH, '/html/body/main/div[2]/div/div/div[2]/div[3]/div/div/ul/table/tbody/tr[2]/td[4]/div/button[4]/i')
        ele_delete.click()
        ele_confirm = driver.find_element(By.XPATH, '/html/body/div[3]/div/div/div[3]/button[2]')
        ele_confirm.click()
        sleep(3)

    def archive_project(self):
        driver = self.driver
        ele_archive = driver.find_element_by_xpath('/html/body/main/div[2]/div/div/div[2]/div[3]/div/div/ul/table/tbody/tr[2]/td[4]/div/button[3]/i')
        ele_archive.click()
        ele_confirm = driver.find_element_by_xpath('/html/body/div[3]/div/div/div[3]/button[2]')
        ele_confirm.click()
        sleep(1)

    def restore_project(self):
        driver = self.driver
        ele_restore = driver.find_element_by_xpath('/html/body/main/div[2]/div/div/div[2]/div[3]/div/div/ul/table/tbody/tr[2]/td[4]/div/button[3]/i')
        ele_restore.click()
        sleep(1)

    def goto_home(self):
        ele_home = self.driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[1]/a[2]/i')
        ele_home.click()
        sleep(2)

    def get_list_file(self):
        driver = self.driver
        element_list = driver.find_element(By.XPATH, '//*[@id="ide-body"]/div[1]/aside[1]/div[1]/file-tree-root/div[3]/ul')
        all_file_list = element_list.find_elements(By.CLASS_NAME, 'item-name-button')
        name_file_list = list(map(lambda n: n.find_element(By.TAG_NAME, 'span').text, all_file_list))
        return name_file_list

    def home_page(self):
        driver = self.driver
        driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[1]/a[2]').click()

    def create_project(self, project_name=""):
        driver = self.driver
        if project_name == "":
            project_name = random_string(10)
        ele_new_project = driver.find_element(By.XPATH, '/html/body/main/div[2]/div/div/div[1]/aside/div[1]/a')
        ele_new_project.click()
        ele_blank_project = driver.find_element(By.XPATH, '/html/body/ul/li[1]/a')
        ele_blank_project.click()
        ele_project_name = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/form/input')
        # print(project_name)
        ele_project_name.send_keys(project_name)
        ele_create = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[3]/button[2]/span[1]')
        ele_create.click()
        sleep(3)
        return driver.current_url

    def copy_project(self):
        driver = self.driver
        copy_project = driver.find_element(By.XPATH, '/html/body/main/div[2]/div/div/div[2]/div[3]/div/div/ul/table/tbody/tr[2]/td[4]/div/button[1]')
        copy_project.click()
        copy_project_confirm = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[3]/button[2]')
        copy_project_confirm.click()
        sleep(3)
        open_copy_project = driver.find_element(By.XPATH, '/html/body/main/div[2]/div/div/div[2]/div[3]/div/div/ul/table/tbody/tr[2]/td[1]/div/span/a')
        open_copy_project.click()
        return driver.current_url

    def invite_to_project(self, guest_username, pri="VIEW"):
        if pri == "VIEW":
            pass
        else:
            pass

    def view_history(self):
        driver = self.driver
        history_link = driver.find_element_by_xpath("/html/body/div[2]/div[3]/div[1]/div[1]/div/editor-navigation-toolbar-root/header/div[3]/a[4]")
        history_link.click()

    def view_history_slice(self):
        driver = self.driver
        history_link = driver.find_element_by_xpath("/html/body/div[2]/div[3]/div[1]/div[1]/div/editor-navigation-toolbar-root/header/div[3]/a[4]")
        history_link.click()
        all_history = driver.find_element_by_xpath("/html/body/div[2]/div[3]/div[1]/div[2]/div[2]/toggle-switch/fieldset/label[1]")
        all_history.click()

    def view_history_slice_edited(self):
        driver = self.driver
        history_link = driver.find_element_by_xpath("/html/body/div[2]/div[3]/div[1]/div[1]/div/editor-navigation-toolbar-root/header/div[3]/a[4]")
        history_link.click()
        all_history = driver.find_element_by_xpath("/html/body/div[2]/div[3]/div[1]/div[2]/div[2]/toggle-switch/fieldset/label[1]")
        all_history.click()

    def view_history_slice_image(self):
        driver = self.driver
        history_link = driver.find_element_by_xpath("/html/body/div[2]/div[3]/div[1]/div[1]/div/editor-navigation-toolbar-root/header/div[3]/a[4]")
        history_link.click()
        all_history = driver.find_element_by_xpath("/html/body/div[2]/div[3]/div[1]/div[2]/div[2]/toggle-switch/fieldset/label[1]")
        all_history.click()
        sleep(3)

    def get_history(self):
        driver = self.driver
        return driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[1]/main/div[2]/div[2]/aside/history-entries-list/div/div')

    def edit_project(self):
        driver = self.driver
        edit_project = driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/main/div[2]/div[1]/div[1]/div[4]/div/div[2]/textarea')
        text = random_string(10)
        edit_project.send_keys(text)
        recompile = driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/main/div[2]/div[1]/div[2]/div/div/preview-pane/div[2]/div[1]/div[1]/button[1]')
        recompile.click()

    def label_version(self):
        driver = self.driver
        label = driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/div[2]/div[1]/button[1]')
        label.click()
        label_name = driver.find_element(By.XPATH, '/html/body/div[4]/div/div/form/div[2]/div[3]/input')
        label_name_input = random_string(10)
        label_name.send_keys(label_name_input)
        add_label = driver.find_element(By.XPATH, '/html/body/div[4]/div/div/form/div[3]/input')
        add_label.click()
        sleep(3)
        return label_name_input

    def label_version_special(self):
        driver = self.driver
        label = driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/div[2]/div[1]/button[1]')
        label.click()
        label_name = driver.find_element(By.XPATH, '/html/body/div[4]/div/div/form/div[2]/div[3]/input')
        label_name_input = 'ベトナム人'
        label_name.send_keys(label_name_input)
        add_label = driver.find_element(By.XPATH, '/html/body/div[4]/div/div/form/div[3]/input')
        add_label.click()
        sleep(3)
        return label_name_input

    def download(self):
        driver = self.driver
        download = driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/div[2]/div[1]/a')
        download.click()

    def compare(self):
        driver = self.driver
        compare = driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/div[2]/div[1]/button[2]')
        compare.click()

    def delete_item(self):
        driver = self.driver
        list_item = driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/main/div[1]/aside[1]/div[1]/file-tree-root/div[3]/ul/li[1]')
        list_item.click()
        delete = driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/main/div[1]/aside[1]/div[1]/file-tree-root/div[2]/div[2]/button[2]')
        delete.click()
        confirm = driver.find_element(By.XPATH, '/html/body/div[4]/div[2]/div/div/div[3]/button[2]')
        confirm.click()

    def restore_item(self):
        driver = self.driver
        restore = driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/div[2]/div[1]/button[2]')
        restore.click()
        sleep(3)
        driver.refresh()
        sleep(3)

    def restore_image(self):
        driver = self.driver
        image = driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/main/div[1]/aside[3]/div/history-file-tree/div/history-file-entity[2]/div')
        image.click()
        sleep(3)
        restore = driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/div[2]/div[1]/button[2]')
        restore.click()
        sleep(3)
        driver.refresh()
        sleep(3)

    def upload_image(self):
        driver = self.driver
        uploadButton = driver.find_element(By.XPATH, '//*[@id="ide-body"]/div[1]/aside[1]/div[1]/file-tree-root/div[2]/div[1]/button[3]/i')
        uploadButton.click()
        uploadFile = driver.find_element(By.XPATH, '/html/body/div[3]/div[2]/div/div/div[2]/table/tbody/tr/td[2]/div/div/div/div[2]/div/div[2]/input[1]')
        uploadFile.send_keys(os.getcwd() + "\\files\\1.png")
        sleep(5)
        name_file_list = self.get_list_file()
        assert '1.png' in name_file_list

    # Khang
    def test_Sharing_S002(self):
        """OL-S-002: Decision table case 2"""
        self.login(USER1)
        project_name = random_string(10)
        project_url = self.create_project(project_name)
        self.invite_to_project(project_url, USER2['username'], pri="VIEW")
        link_edit, link_view = self.turn_on_link_sharing(project_url)
        print(link_view, link_edit)

        # Collaborator
        self.login(USER2)
        self.join_project()
        self.driver.get(project_url)
        sleep(2)
        # Collaborator check view privilege
        assert '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Collaborator check edit privilage (can not)
        assert not '<span class="sr-only">Rename</span>' in self.driver.page_source

        # Anonymous authenticated check view privilege
        self.login(USER3)
        self.driver.get(link_view)
        sleep(2)
        assert '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Anonymous authenticated check edit privilege
        sleep(1)
        self.driver.get(link_edit)
        sleep(3)
        assert '<span class="sr-only">Rename</span>' in self.driver.page_source

    def test_Sharing_S003(self):
        """OL-S-003: Decision table case 3"""
        self.login(USER1)
        project_name = random_string(10)
        project_url = self.create_project(project_name)
        self.invite_to_project(project_url, USER2['username'], pri="EDIT")
        link_edit, link_view = self.turn_on_link_sharing(project_url)
        print(link_view, link_edit)

        # Collaborator
        self.login(USER2)
        self.join_project()
        self.driver.get(project_url)
        sleep(2)
        # Collaborator check view privilege
        assert '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Collaborator check edit privilage (can not)
        assert '<span class="sr-only">Rename</span>' in self.driver.page_source

        # Anonymous authenticated check view privilege
        self.login(USER3)
        self.driver.get(link_view)
        sleep(2)
        assert '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Anonymous authenticated check edit privilege
        sleep(1)
        self.driver.get(link_edit)
        sleep(3)
        assert '<span class="sr-only">Rename</span>' in self.driver.page_source

    def test_Sharing_S004(self):
        """OL-S-004: Decision table case 4"""
        self.login(USER1)
        project_name = random_string(10)
        project_url = self.create_project(project_name)
        link_edit, link_view = self.turn_on_link_sharing(project_url)
        print(link_view, link_edit)

        # Collaborator
        self.login(USER2)
        self.driver.get(project_url)
        sleep(2)
        # Collaborator check view privilege
        assert not '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Collaborator check edit privilage (can not)
        assert not '<span class="sr-only">Rename</span>' in self.driver.page_source

        # Anonymous authenticated check view privilege
        self.login(USER3)
        self.driver.get(link_view)
        sleep(2)
        assert '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Anonymous authenticated check edit privilege
        sleep(1)
        self.driver.get(link_edit)
        sleep(3)
        assert '<span class="sr-only">Rename</span>' in self.driver.page_source

    def test_Sharing_S006(self):
        """OL-S-006: Decision table case 6"""
        self.login(USER3)
        project_name = random_string(10)
        project_url = self.create_project(project_name)
        self.invite_to_project(project_url, USER2['username'], pri="VIEW")
        link_edit, link_view = self.turn_on_link_sharing(project_url)
        print(link_view, link_edit)

        # Collaborator
        self.login(USER2)
        self.join_project()
        self.driver.get(project_url)
        sleep(2)
        # Collaborator check view privilege
        assert '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Collaborator check edit privilage (can not)
        assert not '<span class="sr-only">Rename</span>' in self.driver.page_source

        # Anonymous unauthenticated check view privilege
        self.logout()
        self.driver.get(link_view)
        sleep(2)
        assert '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Anonymous unauthenticated check edit privilege
        sleep(1)
        self.driver.get(link_edit)
        sleep(3)
        assert '<span class="sr-only">Rename</span>' in self.driver.page_source

    def test_Sharing_S007(self):
        """OL-S-007: Decision table case 7"""
        self.login(USER3)
        project_name = random_string(10)
        project_url = self.create_project(project_name)
        self.invite_to_project(project_url, USER2['username'], pri="EDIT")
        link_edit, link_view = self.turn_on_link_sharing(project_url)
        print(link_view, link_edit)

        # Collaborator
        self.login(USER2)
        self.join_project()
        self.driver.get(project_url)
        sleep(2)
        # Collaborator check view privilege
        assert '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Collaborator check edit privilage (can not)
        assert '<span class="sr-only">Rename</span>' in self.driver.page_source

        # Anonymous unauthenticated check view privilege
        self.logout()
        self.driver.get(link_view)
        sleep(2)
        assert '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Anonymous unauthenticated check edit privilege
        sleep(1)
        self.driver.get(link_edit)
        sleep(3)
        assert '<span class="sr-only">Rename</span>' in self.driver.page_source

    def test_Sharing_S008(self):
        """OL-S-008: Decision table case 8"""
        self.login(USER1)
        project_name = random_string(10)
        project_url = self.create_project(project_name)
        link_edit, link_view = self.turn_on_link_sharing(project_url)
        print(link_view, link_edit)

        # Collaborator
        self.login(USER2)
        self.driver.get(project_url)
        sleep(2)
        # Collaborator check view privilege
        assert not '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Collaborator check edit privilage (can not)
        assert not '<span class="sr-only">Rename</span>' in self.driver.page_source

        # Anonymous unauthenticated check view privilege
        self.logout()
        self.driver.get(link_view)
        sleep(2)
        assert '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Anonymous unauthenticated check edit privilege
        sleep(1)
        self.driver.get(link_edit)
        sleep(2)
        assert '<span class="sr-only">Rename</span>' in self.driver.page_source

    def test_Sharing_S010(self):
        """OL-S-010: Decision table case 10"""
        self.login(USER3)
        project_name = random_string(10)
        project_url = self.create_project(project_name)
        self.invite_to_project(project_url, USER2['username'], pri="VIEW")

        # Collaborator
        self.login(USER2)
        self.join_project()
        self.driver.get(project_url)
        sleep(2)
        # Collaborator check view privilege
        assert '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Collaborator check edit privilage (can not)
        assert not '<span class="sr-only">Rename</span>' in self.driver.page_source

    def test_Sharing_S011(self):
        """OL-S-011: Decision table case 11"""
        self.login(USER3)
        project_name = random_string(10)
        project_url = self.create_project(project_name)
        self.invite_to_project(project_url, USER2['username'], pri="EDIT")

        # Collaborator
        self.login(USER2)
        self.join_project()
        self.driver.get(project_url)
        sleep(2)
        # Collaborator check view privilege
        assert '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Collaborator check edit privilage (can not)
        assert '<span class="sr-only">Rename</span>' in self.driver.page_source

    def test_Sharing_S012(self):
        """OL-S-012: Decision table case 12"""
        self.login(USER3)
        project_name = random_string(10)
        project_url = self.create_project(project_name)

        # Collaborator
        self.login(USER2)
        self.join_project()
        self.driver.get(project_url)
        sleep(2)
        # Collaborator check view privilege
        assert not '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Collaborator check edit privilage (can not)
        assert not '<span class="sr-only">Rename</span>' in self.driver.page_source

    def test_Sharing_S014(self):
        """OL-S-014: Decision table case 14"""
        self.login(USER3)
        project_name = random_string(10)
        project_url = self.create_project(project_name)
        self.invite_to_project(project_url, USER2['username'], pri="VIEW")

        # Collaborator
        self.login(USER2)
        self.join_project()
        self.driver.get(project_url)
        sleep(2)
        # Collaborator check view privilege
        assert '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Collaborator check edit privilage (can not)
        assert not '<span class="sr-only">Rename</span>' in self.driver.page_source

    def test_Sharing_S015(self):
        """OL-S-015: Decision table case 15"""
        self.login(USER3)
        project_name = random_string(10)
        project_url = self.create_project(project_name)
        self.invite_to_project(project_url, USER2['username'], pri="EDIT")

        # Collaborator
        self.login(USER2)
        self.join_project()
        self.driver.get(project_url)
        sleep(2)
        # Collaborator check view privilege
        assert '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Collaborator check edit privilage (can not)
        assert '<span class="sr-only">Rename</span>' in self.driver.page_source

    def test_Sharing_S016(self):
        """OL-S-016: Decision table case 16"""
        self.login(USER3)
        project_name = random_string(10)
        project_url = self.create_project(project_name)

        # Collaborator
        self.login(USER2)
        self.join_project()
        self.driver.get(project_url)
        sleep(2)
        assert not '<span class="name">' + project_name + '</span>' in self.driver.page_source
        # Collaborator check edit privilage (can not)
        assert not '<span class="sr-only">Rename</span>' in self.driver.page_source

    # Khoa Ngo
    def test_ActionOfProject_CrP1(self):
        """OL-A-001: Create project with a unique name"""
        self.login(USER1)
        self.create_project()
        assert 'Create Project'

    def create_project_same_name(self):
        driver = self.driver
        ele_new_project = driver.find_element(By.XPATH, '/html/body/main/div[2]/div/div/div[1]/aside/div[1]/a')
        ele_new_project.click()
        ele_blank_project = driver.find_element(By.XPATH, '/html/body/ul/li[1]/a')
        ele_blank_project.click()
        ele_project_name = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/form/input')
        project_name = driver.find_element(By.XPATH, '/html/body/main/div[2]/div/div/div[2]/div[3]/div/div/ul/table/tbody/tr[2]/td[1]/div/span/a').get_attribute('innerHTML')
        ele_project_name.send_keys(project_name)
        sleep(3)
        return driver.current_url

    def download_project(self):
        driver = self.driver
        ele_download = driver.find_element(By.XPATH, '/html/body/main/div[2]/div/div/div[2]/div[3]/div/div/ul/table/tbody/tr[2]/td[4]/div/button[2]/i')
        ele_download.click()
        sleep(3)

    def upload_project(self):
        driver = self.driver
        ele_new_project = driver.find_element_by_xpath('/html/body/main/div[2]/div/div/div[1]/aside/div[1]/a')
        ele_new_project.click()
        ele_upload_project = driver.find_element_by_xpath('/html/body/ul/li[3]/a')
        ele_upload_project.click()
        ele_upload_file = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/div/div/div[2]/input')
        ele_upload_file.send_keys(os.getcwd() + '\\files\\Sample_Project.zip')

    def delete_project(self):
        driver = self.driver
        ele_delete = driver.find_element(By.XPATH, '/html/body/main/div[2]/div/div/div[2]/div[3]/div/div/ul/table/tbody/tr[2]/td[4]/div/button[4]/i')
        ele_delete.click()
        ele_confirm = driver.find_element(By.XPATH, '/html/body/div[3]/div/div/div[3]/button[2]')
        ele_confirm.click()
        sleep(3)

    def archive_project(self):
        driver = self.driver
        ele_archive = driver.find_element_by_xpath('/html/body/main/div[2]/div/div/div[2]/div[3]/div/div/ul/table/tbody/tr[2]/td[4]/div/button[3]/i')
        ele_archive.click()
        ele_confirm = driver.find_element_by_xpath('/html/body/div[3]/div/div/div[3]/button[2]')
        ele_confirm.click()
        sleep(1)

    def restore_project(self):
        driver = self.driver
        ele_restore = driver.find_element_by_xpath('/html/body/main/div[2]/div/div/div[2]/div[3]/div/div/ul/table/tbody/tr[2]/td[4]/div/button[3]/i')
        ele_restore.click()
        sleep(1)

    def goto_home(self):
        ele_home = self.driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[1]/a[2]/i')
        ele_home.click()
        sleep(2)

    def test_ActionOfProject_CrP1(self):
        """OL-A-001: Create project with a unique name"""
        self.login(USER1)
        self.create_project()
        assert 'Create Project'

    def test_ActionOfProject_CrP2(self):
        """OL-A-002: Create project with the same name of another project"""
        self.login(USER1)
        self.create_project_same_name()
        assert 'create Project with same name'

    def test_ActionOfProject_UP(self):
        """OL-A-003: Upload a project"""
        self.login(USER1)
        self.upload_project()
        assert 'Upload Project'

    def test_ActionOfProject_CoP(self):
        """OL-A-004: Copy a project"""
        self.login(USER1)
        self.create_project()
        self.goto_home()
        driver = self.driver
        ele_copy = driver.find_element_by_xpath('/html/body/main/div[2]/div/div/div[2]/div[3]/div/div/ul/table/tbody/tr[2]/td[4]/div/button[1]/i')
        ele_copy.click()
        ele_confirm_copy = driver.find_element_by_xpath('/html/body/div[3]/div/div/div[3]/button[2]/span[1]')
        ele_confirm_copy.click()
        sleep(2)
        assert 'Copy Project'

    def test_ActionOfProject_DoP(self):
        """OL-A-005: Download a project"""
        self.login(USER1)
        self.download_project()
        assert 'Download Project'

    def test_ActionOfProject_EP(self):
        """OL-A-006: Export a project to .pdf file"""
        self.login(USER1)
        self.create_project()
        driver = self.driver
        ele_menu = driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[1]/a[1]')
        ele_menu.click()
        ele_export_pdf = driver.find_element_by_xpath('/html/body/div[2]/aside/ul[1]/li[2]/a/i')
        ele_export_pdf.click()
        sleep(3)
        assert 'Export pdf'

    def test_ActionOfProject_AP(self):
        """OL-A-007: Archive a project"""
        self.login(USER1)
        self.create_project()
        self.goto_home()
        self.archive_project()
        assert 'Archive Project'

    def test_ActionOfProject_RP(self):
        """OL-A-008: Restore an archived project"""
        self.login(USER1)
        self.create_project()
        self.goto_home()
        self.archive_project()
        ele_archive_project_area = self.driver.find_element_by_xpath('/html/body/main/div[2]/div/div/div[1]/aside/div[2]/ul/li[4]')
        ele_archive_project_area.click()
        sleep(1)
        self.restore_project()
        assert 'Restore Project'

    def test_ActionOfProject_DeP(self):
        """OL-A-009: Delete a project"""
        self.login(USER1)
        self.create_project()
        ele_home = self.driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[1]/a[2]/i')
        ele_home.click()
        sleep(3)
        self.delete_project()
        assert 'Delete Project'

    # Long Huynh
    def test_History_VH1(self):
        """OL-H-001: View history"""
        self.login(USER1)
        self.create_project()
        self.view_history()
        assert "Browsing project as of" in self.driver.page_source

    def test_History_VH2(self):
        """OL-H-002: View history 'slice'"""
        self.login(USER1)
        self.create_project()
        self.view_history_slice()
        assert self.driver.find_element_by_class_name('history-entries')

    def test_History_VH3(self):
        """OL-H-003: View copied project history 'slice'"""
        self.login(USER1)
        self.create_project()
        sleep(3)
        self.edit_project()
        sleep(3)
        self.view_history()
        history = self.get_history()
        sleep(3)
        self.home_page()
        sleep(3)
        self.copy_project()
        sleep(3)
        history_link = self.driver.find_element_by_xpath("/html/body/div[2]/div[3]/div[1]/div[1]/div/editor-navigation-toolbar-root/header/div[3]/a[4]")
        history_link.click()
        copy_history = self.driver.find_element_by_xpath("/html/body/div[2]/div[3]/div[1]/main/div[2]/div[2]/aside/history-entries-list/div/div")
        assert copy_history == history

    def test_History_VH4(self):
        """OL-H-004: View text changes"""
        self.login(USER1)
        self.create_project()
        sleep(3)
        self.edit_project()
        sleep(3)
        self.view_history_slice_edited()
        assert "Edited" in self.driver.page_source

    def test_History_VH5(self):
        """OL-H-005: View image and binary changes"""
        self.login(USER1)
        self.create_project()
        sleep(3)
        self.upload_image()
        sleep(3)
        self.view_history_slice_image()
        if "We're still working on showing image and binary changes, sorry. Stay tuned!" in self.driver.page_source:
            raise AssertionError()

    def test_History_LH1(self):
        """OL-H-006: Label a version"""
        self.login(USER1)
        self.create_project()
        sleep(3)
        self.view_history_slice()
        sleep(3)
        label_name = self.label_version()
        assert label_name in self.driver.page_source

    def test_History_LH2(self):
        """OL-H-007: Label a version with special character"""
        self.login(USER1)
        self.create_project()
        sleep(3)
        self.view_history_slice()
        sleep(3)
        label_name = self.label_version_special()
        assert label_name in self.driver.page_source

    def test_History_LH3(self):
        """OL-H-008: Label a version multiple times"""
        self.login(USER1)
        self.create_project()
        sleep(3)
        self.view_history_slice()
        sleep(3)
        label1 = self.label_version()
        label2 = self.label_version()
        assert label1 and label2 in self.driver.page_source

    def test_History_DH1(self):
        """OL-H-009: Download project"""
        self.login(USER1)
        self.create_project()
        sleep(3)
        self.view_history()
        sleep(3)
        self.download()
        assert 'Download Project'

    def test_History_VC1(self):
        """OL-H-010: Compare view"""
        self.login(USER1)
        self.create_project()
        sleep(3)
        self.edit_project()
        sleep(3)
        self.view_history_slice()
        sleep(3)
        self.compare()
        assert self.driver.find_element_by_class_name('annotation-label')

    def test_History_RD1(self):
        """OL-H-011: Recovering deleted text files"""
        self.login(USER1)
        self.create_project()
        sleep(3)
        self.delete_item()
        sleep(3)
        self.view_history()
        sleep(3)
        self.compare()
        self.restore_item()
        assert 'main.tex' in self.get_list_file()

    def test_History_RD2(self):
        """OL-H-012: Recovering deleted images or binary files"""
        self.login(USER1)
        self.create_project()
        sleep(3)
        self.upload_image()
        sleep(3)
        self.delete_item()
        sleep(3)
        self.view_history()
        sleep(3)
        self.compare()
        self.restore_image()
        assert '1.png' in self.get_list_file()

    def test_Comment_001(self):
        PASSED = False
        self.login(USER1)
        self.driver.get(self.PLAYGROUND)

        # Click review button
        sleep(5)
        reviewBtn = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[3]/a[1]")
        reviewBtn.click()

        # Select all document
        sleep(1)
        content = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/main/div[2]/div[1]/div[1]/div[4]/div/div[2]/textarea")
        content.send_keys(Keys.CONTROL, 'a')

        for idx in range(1, 10):
            try:
                # Set parent XPATH
                XPATH = "/html/body/div[2]/div[3]/div[1]/main/div[2]/div[1]/div[1]/div[5]/div[3]/div/div[{}]/div/add-comment-entry/div/div[2]".format(idx)

                # Click new comment button
                sleep(1)
                newCommentBtn = self.driver.find_element(By.XPATH, XPATH + "/a")
                newCommentBtn.click()

                # Enter comment
                sleep(1)
                commentArea = self.driver.find_element(By.XPATH, XPATH + "/div/div[1]/textarea")
                commentArea.click()
                commentArea.send_keys("This is my comment!")

                # Click comment button (to commit)
                sleep(1)
                commentBtn = self.driver.find_element(By.XPATH, XPATH + "/div/div[2]/button[2]")
                commentBtn.click()

                PASSED = True
                break
            except:
                pass

        assert PASSED

    def test_Comment_002(self):
        PASSED = False
        self.login(USER1)
        self.driver.get(self.PLAYGROUND)

        # Click review button
        sleep(5)
        reviewBtn = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[3]/a[1]")
        reviewBtn.click()

        try:
            # Click resolve button of first comment
            sleep(1)
            resolveBtn = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/main/div[2]/div[1]/div[1]/div[5]/div[3]/div/div[1]/div/comment-entry/div/div[3]/div[3]/button[1]")
            resolveBtn.click()
            PASSED = True
        except:
            pass

        assert PASSED

    def test_Comment_003(self):
        PASSED = False
        self.login(USER1)
        self.driver.get(self.PLAYGROUND)

        # Click review button
        sleep(5)
        reviewBtn = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[3]/a[1]")
        reviewBtn.click()

        # Click resolve icon
        sleep(1)
        icon = self.driver.find_element(By.XPATH, '//*[@id="review-panel"]/div[2]/resolved-comments-dropdown/div/a')
        icon.click()

        try:
            # Delete the first resolved comment
            sleep(1)
            deleteBtn = self.driver.find_element(By.XPATH, '//*[@id="review-panel"]/div[2]/resolved-comments-dropdown/div/div[2]/div/resolved-comment-entry[1]/div/div[2]/a[2]')
            deleteBtn.click()
            PASSED = True
        except:
            pass

        assert PASSED

    def test_Comment_004(self):
        PASSED = False
        self.login(USER1)
        self.driver.get(self.PLAYGROUND)

        # Click review button
        sleep(5)
        reviewBtn = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[3]/a[1]")
        reviewBtn.click()

        try:
            # Delete the first comment in first thread
            sleep(1)
            deleteBtn = self.driver.find_element(By.XPATH, '//*[@id="review-panel"]/div[3]/div/div/div/comment-entry/div/div[3]/div[1]/div[1]/div/span[2]/span/a')
            deleteBtn.click()

            # Comfirm
            comfirmBtn = self.driver.find_element(By.XPATH, '//*[@id="review-panel"]/div[3]/div/div/div/comment-entry/div/div[3]/div[1]/div[1]/div/span/a[1]')
            comfirmBtn.click()
            PASSED = True
        except:
            pass

        assert PASSED

    def test_Comment_005(self):
        PASSED = False
        self.login(USER1)
        self.driver.get(self.PLAYGROUND)

        # Click review button
        sleep(5)
        reviewBtn = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[3]/a[1]")
        reviewBtn.click()

        try:
            # Reply the first unresolved comment
            sleep(1)
            textarea = self.driver.find_element(By.XPATH, '//*[@id="review-panel"]/div[3]/div/div/div/comment-entry/div/div[3]/div[2]/textarea')
            textarea.click()
            textarea.send_keys("This is a reply!")
            textarea.send_keys(Keys.RETURN)  # Commit by press Enter
            PASSED = True
        except:
            pass

        assert PASSED

    def test_Comment_006(self):
        PASSED = False
        self.login(USER1)
        self.driver.get(self.PLAYGROUND)

        # Click review button
        sleep(5)
        reviewBtn = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[3]/a[1]")
        reviewBtn.click()

        try:
            # Edit the first comment in first thread
            sleep(1)
            editBtn = self.driver.find_element(By.XPATH, '//*[@id="review-panel"]/div[3]/div/div/div/comment-entry/div/div[3]/div[1]/div[1]/div/span[2]/a')
            editBtn.click()

            textarea = self.driver.find_element(By.XPATH, '//*[@id="review-panel"]/div[3]/div/div/div/comment-entry/div/div[3]/div[1]/div[1]/p/textarea')
            textarea.click()
            textarea.send_keys(Keys.CONTROL, 'a')
            textarea.send_keys(Keys.DELETE)
            textarea.send_keys("This comment had been edited!")
            textarea.send_keys(Keys.RETURN)  # Commit by Enter
            PASSED = True
        except:
            pass

        assert PASSED


if __name__ == "__main__":
    unittest.main()
