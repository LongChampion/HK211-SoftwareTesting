from testcase import TestCase
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from time import sleep


class OL_C_005(TestCase):
    def __init__(self):
        TestCase.__init__(self)
        print("[*] Testcase created!")

    def run(self):
        self.login()
        self.driver.get(self.PLAYGROUND)

        # Click review button
        sleep(5)
        reviewBtn = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[3]/a[1]")
        reviewBtn.click()

        try:
            # Reply the first unresolved comment
            sleep(1)
            textarea = self.driver.find_element(By.XPATH, '//*[@id="review-panel"]/div[3]/div/div/div/comment-entry/div/div[3]/div[2]/textarea')
            textarea.click()
            textarea.send_keys("This is a reply!")
            textarea.send_keys(Keys.RETURN)  # Commit by press Enter
        except:
            raise Exception("[-] No comment to reply!")

        print("[+] Testcase completed!")
