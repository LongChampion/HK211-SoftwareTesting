from testcase import TestCase
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from time import sleep


class OL_C_002(TestCase):
    def __init__(self):
        TestCase.__init__(self)
        print("[*] Testcase created!")

    def run(self):
        self.login()
        self.driver.get(self.PLAYGROUND)

        # Click review button
        sleep(5)
        reviewBtn = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[3]/a[1]")
        reviewBtn.click()

        try:
            # Click resolve button of first comment
            sleep(1)
            resolveBtn = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/main/div[2]/div[1]/div[1]/div[5]/div[3]/div/div[1]/div/comment-entry/div/div[3]/div[3]/button[1]")
            resolveBtn.click()

        except:
            raise Exception("[-] No comment to resolve!")

        print("[+] Testcase completed!")
