from testcase import TestCase
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from time import sleep


class OL_C_006(TestCase):
    def __init__(self):
        TestCase.__init__(self)
        print("[*] Testcase created!")

    def run(self):
        self.login()
        self.driver.get(self.PLAYGROUND)

        # Click review button
        sleep(5)
        reviewBtn = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/div[1]/div/div/editor-navigation-toolbar-root/header/div[3]/a[1]")
        reviewBtn.click()

        try:
            # Edit the first comment in first thread
            sleep(1)
            editBtn = self.driver.find_element(By.XPATH, '//*[@id="review-panel"]/div[3]/div/div/div/comment-entry/div/div[3]/div[1]/div[1]/div/span[2]/a')
            editBtn.click()

            textarea = self.driver.find_element(By.XPATH, '//*[@id="review-panel"]/div[3]/div/div/div/comment-entry/div/div[3]/div[1]/div[1]/p/textarea')
            textarea.click()
            textarea.send_keys(Keys.CONTROL, 'a')
            textarea.send_keys(Keys.DELETE)
            textarea.send_keys("This comment had been edited!")
            textarea.send_keys(Keys.RETURN)  # Commit by Enter

        except:
            raise Exception("[-] No comment to edit!")

        print("[+] Testcase completed!")
